﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// These objects are a collection of MazeCells that form the rooms in a dungeon.
/// </summary>
public class MazeRoom {
    /// <summary>
    /// _cells : contains all of the cells in the room
    /// _regions : contains all of the regions that the room is connected to
    /// _x, _y : are the co-ordinates of the room
    /// _width, _height : are the width and height of the room
    /// </summary>

    public int x { get; private set; }
    public int y { get; private set; }
    public int width { get; private set; }
    public int height { get; private set; }

    public MazeRoom(int x, int y, int width, int height)
    {
        cells = new List<MazeCell>();
        passageways = new List<MazePassageway>();

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public List<MazeCell> cells { get; private set; }
    public List<MazePassageway> passageways { get; private set; }

    /// <summary>
    /// This method goes through every cell in a room and checks if the cell is part of the outer edge of the room, if it is then it destroys every wall of the cell apart from the room that
    /// forms the rooms walls.
    /// </summary>
    public void FindOuterRoomCells()
    {
        foreach (MazeCell cell in cells)
        {
            if (cell.y != y)
                DestroyIfExists(cell.northWall);

            if (cell.y != y + height)
                DestroyIfExists(cell.southWall);

            if (cell.x != x)
                DestroyIfExists(cell.westWall);

            if (cell.x != x + width)
                DestroyIfExists(cell.eastWall);
        }
    }

    private void DestroyIfExists(MazeCell.MazeObject mazeObject)
    {
        if (mazeObject != null)
            mazeObject.exists = false;
    }
}
