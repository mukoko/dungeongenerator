﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Objects of this class hold information on a Passageway and Room pair, 
/// It contains information on the passageway and room that are connected together and which cell(s) connect the two together
/// </summary>
public class MazeConnector {

    public MazeConnector(MazeCell passagewayCell, MazeCell roomCell, MazeRoom room, MazePassageway passageway)
    {
        this.passagewayCell = passagewayCell;
        this.roomCell = roomCell;
        this.room = room;
        this.passageway = passageway;
    }

    public MazeCell passagewayCell { get; private set; }
    public MazeCell roomCell { get; private set; }
    public MazeRoom room { get; private set; }
    public MazePassageway passageway { get; private set; }
}
