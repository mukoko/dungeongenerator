﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour {
    private bool _generating = false;

    public void Generate()
    {
        if (!_generating)
        {
            _generating = true;
            SceneManager.LoadScene(0);
        }
    }
}
