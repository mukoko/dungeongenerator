﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This holds information on every room and region that is connected to the final dungeon being generated
/// </summary>
public class MazeConnectionManager {
    /// <summary>
    /// Variables:
    /// _mainPassageway : is the first passageway used when connecting rooms to regions.
    /// This is the passageway that allows a connection to the most amount of rooms
    /// 
    /// _mainRooms : These are the rooms that allow a connection to the main passageway
    /// _connectedRooms : These are the rooms that are connection to a room or passageway that is connected to the main passageway
    /// _subPassageways : These are the passageways that are connected to the mainRegion through a room connected to the mainPassageway
    /// _usedConnectors : are all of the passageway/room pairs that are used to connected passageways and rooms to the main passageway/sub passageway.
    /// </summary>
    /// 
    public MazeConnectionManager(MazePassageway pasageway, HashSet<MazeRoom> mainRooms)
    {
        mainPassageway = pasageway;
        this.mainRooms = mainRooms;
        connectedRooms = new List<MazeRoom>();
        subPassageways = new HashSet<MazePassageway>();
        usedConnectors = new List<MazeConnector>();
    }

    public MazePassageway mainPassageway { get; private set; }
    public HashSet<MazeRoom> mainRooms { get; private set; }
    public List<MazeRoom> connectedRooms { get; private set; }
    public HashSet<MazePassageway> subPassageways { get; private set; }
    public List<MazeConnector> usedConnectors { get; private set; }
}
