﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class MazeLoader : MonoBehaviour {

    [SerializeField]
    private int _oldAdditionPercent = 10;

    [SerializeField]
    private int _mazeColumns;
    public int mazeColumns
    {
        get { return _mazeColumns; }
    }

    [SerializeField]
    private int _mazeRows;
    public int mazeRows
    {
        get { return _mazeRows; }
    }

    [SerializeField]
    private int numberOfAttempts = 1000;

    [SerializeField]
    private GameObject _wallObject;

    [SerializeField]
    private GameObject _floorObject;

    private List<MazeRoom> _rooms;
    private MazeConnectionManager _connectionManager;

    private MazeAlgorithm _mazeAlgorithm;
    private MazeCell[,] _mazeCells;
    public MazeCell[,] mazeCells
    {
        get { return _mazeCells; }
    }

    private bool _connectorSearchComplete;
    private bool _establishMainRoute;
    private bool _dealWithMainConnection;
    private bool _dealWithOtherConnections;
    private bool _addedWalls;

    private int _iteratorPosition = 0;

    private Transform _floorHolder, _northWallHolder, _southWallHolder, _westWallHolder, _eastWallHolder, _tempObjectHolder, _outerWallHolder;

    // Use this for initialization
    void Awake () {
        InitialiseHolders();
        InitialiseMaze();

        if (_mazeAlgorithm == null)
            _mazeAlgorithm = new MazeAlgorithm(_mazeCells, _mazeColumns, _mazeRows);

        _rooms = new List<MazeRoom>();

        for (int i = 0; i < numberOfAttempts; i++)
        {
            int rSize = Random.Range(1, 4) * 2;
            int rectangular = Random.Range(0, rSize / 2);

            int width = rSize;
            int height = rSize;

            if (Random.Range(0, 2) == 0)
                width += rectangular;
            else
                height += rectangular;

            int x = Random.Range(0, ((_mazeColumns - width) / 2)) * 2 + 1;
            int y = Random.Range(0, ((_mazeRows - height) / 2)) * 2 + 1;

            //At this point a new square/rectangle is created at position x/y.
            //A check is made to see if the room is within the grid.
            MazeRoom room = new MazeRoom(x, y, width, height);
            foreach (MazeCell cell in _mazeCells)
            {
                if (cell.x >= x && cell.x <= x + width && cell.y >= y && cell.y <= y + height)
                    room.cells.Add(cell);
            }

            bool overlap = false;

            Rect currentRect = new Rect(x, y, width, height);
            foreach (MazeRoom otherRoom in _rooms)
            {
                Rect otherRect = new Rect(otherRoom.x, otherRoom.y, otherRoom.width, otherRoom.height);
                if (Overlaps(otherRect, currentRect, 1))
                {
                    overlap = true;
                    break;
                }
            }

            if (overlap) continue;

            //No overlaps then add the room, and carve it out.
            _rooms.Add(room);
            foreach (MazeCell cell in room.cells)
            {
                cell.visited = true;
                cell.room = room;
            }

            room.FindOuterRoomCells();
        }    

        while (!_mazeAlgorithm.mazeComplete)
            _mazeAlgorithm.Kill();

        while (!_connectorSearchComplete)
            SearchForPassagewayConnections();

        while (!_establishMainRoute)
            EstablishMainRoute();

        while (!_dealWithMainConnection)
            DealWithMainConnection();

        while (!_dealWithOtherConnections)
            DealWithOtherConnections();
        
        while (!_addedWalls)
            AddWalls();
    }

    //A square is not intersecting when rA's left edge is to the right of rB's right edge
    //A square is not intersecting when rA's right edge is to the left of rB's left edge
    //A square is not intersecting when rA's top edge is to the bottom of rB's bottom edge
    //A square is not intersecting when rA's bottom edge is to the top of rB's top edge
    bool Overlaps(Rect rA, Rect rB, int distance)
    {
        if (rA.x - distance > rB.xMax || rA.xMax + distance < rB.x || rA.y - distance > rB.yMax || rA.yMax + distance < rB.y) return false;
        return true;
    }

    /// <summary>
    /// The methods in the Initialiser Region deal with creating the initial grid, and the game objects that hold the maze objects.
    /// </summary>
    #region Initialisers
    private void InitialiseHolders()
    {
        _floorHolder = new GameObject("Floors").transform;
        _floorHolder.SetParent(gameObject.transform);

        _northWallHolder = new GameObject("North Wall Holder").transform;
        _northWallHolder.SetParent(gameObject.transform);

        _southWallHolder = new GameObject("South Wall Holder").transform;
        _southWallHolder.SetParent(gameObject.transform);

        _eastWallHolder = new GameObject("East Wall Holder").transform;
        _eastWallHolder.SetParent(gameObject.transform);

        _westWallHolder = new GameObject("West Wall Holder").transform;
        _westWallHolder.SetParent(gameObject.transform);
    }

    /// <summary>
    /// This method creates the grid object (not Unity's GameObject), each grid cell has a floor, east wall, south wall.
    /// If the grid cell's column == 0 then it will have a west wall.
    /// If the grid cell's row == 0 then it will have a north wall.
    /// </summary>
    private void InitialiseMaze()
    {
        _mazeCells = new MazeCell[_mazeColumns, _mazeRows];

        for (int x = 0; x < _mazeColumns; x++)
        {
            for (int y = 0; y < _mazeRows; y++)
            {
                _mazeCells[x, y] = new MazeCell(x, y);

                _mazeCells[x, y].floor = new MazeCell.MazeObject(_mazeCells[x, y], Direction.Floor);

                if (x == 0)
                    _mazeCells[x, y].westWall = new MazeCell.MazeObject(_mazeCells[x, y], Direction.West);
                _mazeCells[x, y].eastWall = new MazeCell.MazeObject(_mazeCells[x, y], Direction.East);

                if (y == 0)
                    _mazeCells[x, y].northWall = new MazeCell.MazeObject(_mazeCells[x, y], Direction.North);
                _mazeCells[x, y].southWall = new MazeCell.MazeObject(_mazeCells[x, y], Direction.South);
            }
        }
    }
    #endregion

    #region Main Dungeon Builder

    /// <summary>
    /// This method iterates through each passageway generated by the Maze Generator Algorithm.
    /// It checks if the region has cells that are adjacent to atleast two rooms.
    /// If not then the region is removed for the generator.
    /// There is a rare circumstance, where a room will not connect to any valid region, if this is the case then the room is destroyed.
    /// </summary>
    private void SearchForPassagewayConnections()
    {
        MazePassageway currentPassageway = _mazeAlgorithm.passageways[_iteratorPosition];
        if (!HasMultipleLinks(currentPassageway))
            currentPassageway.SetUnused();

        _iteratorPosition++;
        if (_iteratorPosition == _mazeAlgorithm.passageways.Count)
        {

            List<MazeRoom> roomsToKeep = new List<MazeRoom>();
            for (int i = _mazeAlgorithm.passageways.Count - 1; i >= 0; i--)
            {
                MazePassageway passageway = _mazeAlgorithm.passageways[i];
                if (passageway.unused)
                    _mazeAlgorithm.passageways.RemoveAt(i);
                else
                {
                    foreach (MazeRoom room in passageway.passagewayRooms)
                    {
                        if (!roomsToKeep.Contains(room))
                            roomsToKeep.Add(room);
                    }
                }
            }



            for (int i = _rooms.Count - 1; i >= 0; i--)
            {
                if (!roomsToKeep.Contains(_rooms[i]))
                {
                    foreach (MazeCell cell in _rooms[i].cells)
                    {
                        cell.unused = true;
                        _rooms.RemoveAt(i);
                    }
                }
            }

            _iteratorPosition = 0;
            _connectorSearchComplete = true;
        }
    }

    /// <summary>
    /// This method takes a passageway and checks if it has links to atleast two rooms.
    /// If so then a connector object is added to the region, which contains information on the cell in the region and the cell in the adjacent cell (in the room) that connect the region and room together.
    /// A region can have multiple connectors to the same room.
    /// </summary>
    /// <param name="passageway"></param>
    /// <returns>Whether the passage has atleast links to atleast two rooms</returns>
    private bool HasMultipleLinks(MazePassageway passageway)
    {
        HashSet<MazeRoom> checkedRooms = new HashSet<MazeRoom>();
        foreach (MazeCell cell in passageway.passagewayCells)
        {
            if (cell.x > 0)
            {
                MazeCell potential = _mazeCells[cell.x - 1, cell.y];
                if (potential.room != null)
                {
                    checkedRooms.Add(potential.room);
                    passageway.connectorCells.Add(new MazeConnector(cell, potential, potential.room, passageway));
                    passageway.passagewayRooms.Add(potential.room);
                    potential.room.passageways.Add(passageway);
                }
            }

            if (cell.x < _mazeColumns - 1)
            {
                MazeCell potential = _mazeCells[cell.x + 1, cell.y];
                if (potential.room != null)
                {
                    checkedRooms.Add(potential.room);
                    passageway.connectorCells.Add(new MazeConnector(cell, potential, potential.room, passageway));
                    passageway.passagewayRooms.Add(potential.room);
                    potential.room.passageways.Add(passageway);
                }
            }

            if (cell.y > 0)
            {
                MazeCell potential = _mazeCells[cell.x, cell.y - 1];
                if (potential.room != null)
                {
                    checkedRooms.Add(potential.room);
                    passageway.connectorCells.Add(new MazeConnector(cell, potential, potential.room, passageway));
                    passageway.passagewayRooms.Add(potential.room);
                    potential.room.passageways.Add(passageway);
                }
            }

            if (cell.y < _mazeRows - 1)
            {
                MazeCell potential = _mazeCells[cell.x, cell.y + 1];
                if (potential.room != null)
                {
                    checkedRooms.Add(potential.room);
                    passageway.connectorCells.Add(new MazeConnector(cell, potential, potential.room, passageway));
                    passageway.passagewayRooms.Add(potential.room);
                    potential.room.passageways.Add(passageway);
                }
            }
        }

        return checkedRooms.Count >= 2;
    }

    /// <summary>
    /// This finds the passageway that has the most connections to the rooms in the generator.
    /// The room that is found is declared as the main room.
    /// </summary>
    private void EstablishMainRoute()
    {
        MazePassageway passagewayWithMostRooms = _mazeAlgorithm.passageways[0];
        HashSet<MazeRoom> passagewayRoomSet = new HashSet<MazeRoom>();

        foreach (MazeConnector mazeConnector in passagewayWithMostRooms.connectorCells)
            passagewayRoomSet.Add(mazeConnector.room);

        for (int i = 1; i < _mazeAlgorithm.passageways.Count; i++)
        {
            HashSet<MazeRoom> uniqueRoom = new HashSet<MazeRoom>();
            foreach (MazeConnector connector in _mazeAlgorithm.passageways[i].connectorCells)
                uniqueRoom.Add(connector.room);

            if (uniqueRoom.Count > passagewayRoomSet.Count)
            {
                passagewayWithMostRooms = _mazeAlgorithm.passageways[i];
                passagewayRoomSet = uniqueRoom;
            }
        }

        _connectionManager = new MazeConnectionManager(passagewayWithMostRooms, passagewayRoomSet);
        _establishMainRoute = true;
    }

    /// <summary>
    /// This method takes the main passageway, and proceeds to connect every room that has a connection to this passageway to it.
    /// It will take a random connector that the room has to the passageway and cut a hole between the room and passageway.
    /// Once all the rooms are connected the redundant rooms are declared Unused.
    /// The redudant rooms are the rooms that do not cause a break in a path from the first room connected to the passageway to the last.
    /// </summary>
    private void DealWithMainConnection()
    {
        MazePassageway mainPassageway = _connectionManager.mainPassageway;
        List<MazeConnector> mainConnectors = mainPassageway.connectorCells;

        int random = Random.Range(0, mainConnectors.Count);
        MazeConnector connector = mainConnectors[random];
        mainConnectors.Remove(connector);

        if (_connectionManager.connectedRooms.Contains(connector.room))
            return;

        FindAndDestroyWallBetween(connector.passagewayCell, connector.roomCell, true);
        _connectionManager.usedConnectors.Add(connector);
        _connectionManager.connectedRooms.Add(connector.room);

        bool allRoomsConnected = true;
        foreach (MazeRoom room in _connectionManager.mainRooms)
        {
            if (!_connectionManager.connectedRooms.Contains(room))
            {
                allRoomsConnected = false;
                break;
            }
        }

        if (allRoomsConnected)
        {
            List<MazeConnector> connectors = new List<MazeConnector>();
            foreach (MazeConnector conn in _connectionManager.usedConnectors)
            {
                if (conn.passageway == mainPassageway)
                    connectors.Add(conn);
            }

            if (_connectionManager.mainPassageway.passagewayCells.Count == 1)
                return;

            int smallIndex = _connectionManager.mainPassageway.passagewayCells.Count;
            int largeIndex = 0;

            foreach (MazeConnector connct in connectors)
            {
                if (_connectionManager.mainPassageway.passagewayCells.IndexOf(connct.passagewayCell) < smallIndex)
                    smallIndex = _connectionManager.mainPassageway.passagewayCells.IndexOf(connct.passagewayCell);

                if (_connectionManager.mainPassageway.passagewayCells.IndexOf(connct.passagewayCell) > largeIndex)
                    largeIndex = _connectionManager.mainPassageway.passagewayCells.IndexOf(connct.passagewayCell);
            }

            List<MazeCell> cellsToKeep = _connectionManager.mainPassageway.passagewayCells.GetRange(smallIndex, largeIndex - smallIndex + 1);
            foreach (MazeCell cell in _connectionManager.mainPassageway.passagewayCells)
            {
                if (!cellsToKeep.Contains(cell))
                    cell.unused = true;
            }

            for (int i = 0; i < cellsToKeep.Count - 1; i++)
                FindAndDestroyWallBetween(cellsToKeep[i], cellsToKeep[i + 1], false);

            _dealWithMainConnection = true;
        }
    }

    /// <summary>
    /// This method deals with connecting the remainder of rooms the a passageway, the passageways are now found by using a room already connected to the main connection.
    /// A random room that is already connected is selected. If the room has any passageways that hasn't already been used to connect other rooms (including itself) then one of those passageways are used.
    /// There is a chance that the passageway is discarded if it does not connect any new rooms to the main connection.
    /// Every room on the passageway then goes through the process that the main passageway had done previously.
    /// </summary>
    private void DealWithOtherConnections()
    {

        //All of rooms have been connected, time to remove any unused cells.
        if (_rooms.Count == _connectionManager.connectedRooms.Count)
        {
            foreach (MazePassageway passageway in _mazeAlgorithm.passageways)
            {
                if (passageway != _connectionManager.mainPassageway && !_connectionManager.subPassageways.Contains(passageway))
                {
                    foreach (MazeCell cell in passageway.passagewayCells)
                        cell.unused = true;
                }
            }

            foreach (MazeCell cell in _mazeCells)
            {
                if (cell.unused)
                    DealWithUnusedCell(cell);
            }

            _dealWithOtherConnections = true;
            return;
        }

        bool selectedNextRoom = false;
        MazeRoom selectedRoom = null;
        MazePassageway selectedPassageway = null;

        if (!selectedNextRoom)
        {
            //Randomly find a room that has been connected
            int rNumber = Random.Range(0, _connectionManager.connectedRooms.Count);
            selectedRoom = _connectionManager.connectedRooms[rNumber];

            bool newAddition = false;

            //Find whether the room is adjacent to any passageways that hasn't been add to the main/sub passageways.
            foreach (MazePassageway passageway in selectedRoom.passageways)
            {
                if (!_connectionManager.subPassageways.Contains(passageway) && passageway != _connectionManager.mainPassageway)
                {
                    //Check if the passageway has any rooms that is not connected
                    selectedPassageway = passageway;
                    foreach (MazeRoom room in passageway.passagewayRooms)
                    {
                        if (!_connectionManager.connectedRooms.Contains(room))
                            newAddition = true;
                    }

                    break;
                }
            }

            //If the passageway doesn't add any new rooms then there is a chance that you will delete a room.
            if (!newAddition && selectedPassageway != null)
            {
                if (Random.Range(1, 11) < _oldAdditionPercent)
                    newAddition = true;
                else
                {
                    foreach (MazeCell cell in selectedPassageway.passagewayCells)
                        cell.unused = true;

                    _mazeAlgorithm.passageways.Remove(selectedPassageway);
                    return;
                }
            }

            if (newAddition)
            {
                _connectionManager.subPassageways.Add(selectedPassageway);
            }
        }

        if (selectedRoom == null || selectedPassageway == null) return;

        //Find all of the rooms on the passageway that are not the room you selected
        List<MazeRoom> remainingRooms = new List<MazeRoom>();
        foreach (MazeRoom room in selectedPassageway.passagewayRooms)
        {
            if (room != selectedRoom)
                remainingRooms.Add(room);
        }

        if (remainingRooms.Count == 0) return;

        //Randomly choose a connector between the selected room and the passageway
        List<MazeConnector> selectedRoomConnectors = new List<MazeConnector>();
        foreach (MazeConnector connector in selectedPassageway.connectorCells)
        {
            if (connector.room == selectedRoom)
                selectedRoomConnectors.Add(connector);
        }

        if (selectedRoomConnectors.Count == 0) return;

        MazeConnector roomConnector = selectedRoomConnectors[Random.Range(0, selectedRoomConnectors.Count)];

        //Destroy the wall between the passageway and selected room via the connector.
        FindAndDestroyWallBetween(roomConnector.passagewayCell, roomConnector.roomCell, true);
        _connectionManager.usedConnectors.Add(roomConnector);

        int numberConnected = 0;
        int numberNotConnected = 0;

        //Get the number of rooms that are/not connected
        foreach (MazeRoom room in remainingRooms)
        {
            if (_connectionManager.connectedRooms.Contains(room))
                numberConnected++;
            else numberNotConnected++;
        }

        //If all of the rooms are already connected then randomly choose one of these rooms to connect to the selected room via the passageway
        HashSet<MazeRoom> toConnect = new HashSet<MazeRoom>();
        if (numberConnected > 0 && numberNotConnected == 0)
        {
            toConnect.Add(remainingRooms[Random.Range(0, remainingRooms.Count)]);
        }
        else
        {
            //Otherwise connect all of the rooms together via the passageway
            foreach (MazeRoom room in remainingRooms)
            {
                if (!_connectionManager.connectedRooms.Contains(room))
                {
                    toConnect.Add(room);
                }
            }
        }

        //Randomly choose a connector between the rooms to connect and the passageway, and join the two
        foreach (MazeRoom room in toConnect)
        {
            List<MazeConnector> c = new List<MazeConnector>();
            foreach (MazeConnector connector in selectedPassageway.connectorCells)
            {
                if (connector.room == room)
                    c.Add(connector);
            }


            MazeConnector rConnector = c[Random.Range(0, c.Count)];

            FindAndDestroyWallBetween(rConnector.passagewayCell, rConnector.roomCell, true);

            _connectionManager.usedConnectors.Add(rConnector);
            if (!_connectionManager.connectedRooms.Contains(room))
                _connectionManager.connectedRooms.Add(room);
        }

        List<MazeConnector> connectors = new List<MazeConnector>();
        foreach (MazeConnector con in _connectionManager.usedConnectors)
        {
            if (con.passageway == selectedPassageway)
                connectors.Add(con);
        }

        if (selectedPassageway.passagewayCells.Count == 1)
            return;

        //Remove uneceesary cells

        int smallIndex = selectedPassageway.passagewayCells.Count;
        int largeIndex = 0;

        foreach (MazeConnector connect in connectors)
        {
            if (selectedPassageway.passagewayCells.IndexOf(connect.passagewayCell) < smallIndex)
                smallIndex = selectedPassageway.passagewayCells.IndexOf(connect.passagewayCell);

            if (selectedPassageway.passagewayCells.IndexOf(connect.passagewayCell) > largeIndex)
                largeIndex = selectedPassageway.passagewayCells.IndexOf(connect.passagewayCell);
        }

        List<MazeCell> cellsToKeep = selectedPassageway.passagewayCells.GetRange(smallIndex, largeIndex - smallIndex + 1);
        foreach (MazeCell cell in selectedPassageway.passagewayCells)
        {
            if (!cellsToKeep.Contains(cell))
                cell.unused = true;
        }

        for (int i = 0; i < cellsToKeep.Count - 1; i++)
            FindAndDestroyWallBetween(cellsToKeep[i], cellsToKeep[i + 1], false);
    }

    /// <summary>
    /// Adds Unity GameObjects to the valid walls/floors left in the generator.
    /// </summary>
    private void AddWalls()
    {
        foreach (MazeCell cell in _mazeCells)
        {
            if (WallExists(cell.floor))
            {
                cell.floor.mazeObject = Instantiate(_floorObject, new Vector3(cell.x, cell.y), Quaternion.identity);
                cell.floor.mazeObject.transform.SetParent(_floorHolder);
                cell.floor.mazeObject.name = "Cell (" + cell.x + ", " + cell.y + ")";
            }

            if (WallExists(cell.northWall))
            {
                cell.northWall.mazeObject = Instantiate(_wallObject, new Vector3(cell.x, cell.y - 0.5f), Quaternion.identity);
                cell.northWall.mazeObject.transform.SetParent(_northWallHolder);
                cell.northWall.mazeObject.name = "North Wall (" + cell.x + ", " + cell.y + ")";
            }

            if (WallExists(cell.southWall))
            {
                cell.southWall.mazeObject = Instantiate(_wallObject, new Vector3(cell.x, cell.y + 0.5f), Quaternion.identity);
                cell.southWall.mazeObject.transform.SetParent(_southWallHolder);
                cell.southWall.mazeObject.name = "South Wall (" + cell.x + ", " + cell.y + ")";
            }

            if (WallExists(cell.eastWall))
            {
                cell.eastWall.mazeObject = Instantiate(_wallObject, new Vector3(cell.x + 0.5f, cell.y), Quaternion.identity);
                cell.eastWall.mazeObject.transform.Rotate(Vector3.forward * 90f);
                cell.eastWall.mazeObject.transform.SetParent(_eastWallHolder);
                cell.eastWall.mazeObject.name = "East Wall (" + cell.x + ", " + cell.y + ")";
            }

            if (WallExists(cell.westWall))
            {
                cell.westWall.mazeObject = Instantiate(_wallObject, new Vector3(cell.x - 0.5f, cell.y), Quaternion.identity);
                cell.westWall.mazeObject.transform.Rotate(Vector3.forward * 90f);
                cell.westWall.mazeObject.transform.SetParent(_westWallHolder);
                cell.westWall.mazeObject.name = "West Wall (" + cell.x + ", " + cell.y + ")";
            }
        }

        _addedWalls = true;
    }

    #endregion

    #region Searching and Destroying Walls

    private void FindAndDestroyWallBetween(MazeCell cellOne, MazeCell cellTwo, bool addEntrance)
    {
        if (cellOne.unused || cellTwo.unused)
            return;

        if (cellOne.x == cellTwo.x)
        {
            if (cellOne.y > cellTwo.y)
            {
                DestroyIfExists(cellOne.northWall);
                DestroyIfExists(cellTwo.southWall);
            }
            else if (cellOne.y < cellTwo.y)
            {
                DestroyIfExists(cellTwo.northWall);
                DestroyIfExists(cellOne.southWall);
            }
        }

        else if (cellOne.y == cellTwo.y)
        {
            if (cellOne.x > cellTwo.x)
            {
                DestroyIfExists(cellOne.westWall);
                DestroyIfExists(cellTwo.eastWall);
            }
            else if (cellOne.x < cellTwo.x)
            {
                DestroyIfExists(cellTwo.westWall);
                DestroyIfExists(cellOne.eastWall);
            }
        }
    }

    /// <summary>
    /// Will destroy the walls of a cell if the cell is not needed
    /// </summary>
    /// <param name="cell"></param>
    private void DealWithUnusedCell(MazeCell cell)
    {
        DestroyIfExists(cell.floor);

        if (cell.x > 0)
        {
            MazeCell otherCell = _mazeCells[cell.x - 1, cell.y];
            if (otherCell.unused)
            {
                DestroyIfExists(cell.westWall);
                DestroyIfExists(otherCell.eastWall);
            }
        }
        else if (cell.x == 0)
        {
            DestroyIfExists(cell.westWall);
        }


        if (cell.x < _mazeColumns - 1)
        {
            MazeCell otherCell = _mazeCells[cell.x + 1, cell.y];
            if (otherCell.unused)
            {
                DestroyIfExists(cell.eastWall);
                DestroyIfExists(otherCell.westWall);
            }
        }
        else if (cell.x == _mazeColumns - 1)
        {
            DestroyIfExists(cell.eastWall);
        }

        if (cell.y > 0)
        {
            MazeCell otherCell = _mazeCells[cell.x, cell.y - 1];
            if (otherCell.unused)
            {
                DestroyIfExists(cell.northWall);
                DestroyIfExists(otherCell.southWall);
            }
        }
        else if (cell.y == 0)
        {
            DestroyIfExists(cell.northWall);
        }

        if (cell.y < _mazeRows - 1)
        {
            MazeCell otherCell = _mazeCells[cell.x, cell.y + 1];
            if (otherCell.unused)
            {
                DestroyIfExists(cell.southWall);
                DestroyIfExists(otherCell.northWall);
            }
        }
        else if (cell.y == _mazeRows - 1)
        {
            DestroyIfExists(cell.southWall);
        }
    }

    protected void DestroyIfExists(MazeCell.MazeObject obj)
    {
        if (WallExists(obj))
            obj.exists = false;
    }

    protected bool WallExists(MazeCell.MazeObject wall)
    {
        if (wall != null)
        {
            return wall.exists;
        }

        return false;
    }

    #endregion
}
