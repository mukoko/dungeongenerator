﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A passageway is formed of Maze Cells that were not part of a room, 
/// The Maze Generator is performed on those cells, and a region is made up of all the cells that are selected during one cycle of the algorithm (All the kill methods before a single hunt method)
/// </summary>
public class MazePassageway {

    /// <summary>
    /// Variables:
    /// _regionCells : All list of Maze Cells that make up the region
    /// _connectorCells: All of the Maze Cells in a region that allow a connection to are room (they are adjacent to a room)
    /// _passagewayRooms : All of the rooms that are connected to this region
    /// _unused : A region is set to unused if it is to be discarded by the generator
    /// _passagewayColor : Each region is set a random color so it is easy to distingush a region.
    /// </summary>

    public List<MazeCell> passagewayCells { get; private set; }
    public List<MazeConnector> connectorCells { get; private set; }
    public HashSet<MazeRoom> passagewayRooms { get; private set; }
    public Color passagewayColor { get; private set; }
    public bool unused { get; private set; }

    public MazePassageway()
    {
        passagewayCells = new List<MazeCell>();
        connectorCells = new List<MazeConnector>();
        passagewayRooms = new HashSet<MazeRoom>();
        passagewayColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    }

    /// <summary>
    /// Called whenever a region is set to be discarded
    /// </summary>
    public void SetUnused()
    {
        unused = true;
        foreach (MazeCell cell in passagewayCells)
            cell.unused = true;

        passagewayColor = Color.black;
    }

 
    public void AddCell(MazeCell cell)
    {
        passagewayCells.Add(cell);
        cell.passageway = this;
    }
}
