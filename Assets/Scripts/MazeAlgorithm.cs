﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is an implementation of Jamis Buck's HUNT AND KILL Algorithm
/// http://weblog.jamisbuck.org/2011/1/24/maze-generation-hunt-and-kill-algorithm
/// </summary>
public class MazeAlgorithm {
    private int _currentX = 0;
    private int _currentY = 0;
    private int _mazeColumns;
    private int _mazeRows;

    private MazeCell[,] _cells;


    public bool mazeComplete { get; private set; }
    public List<MazePassageway> passageways { get; private set; }

    private MazePassageway _currentPassageway;

    /// <summary>
    /// Initialisation...
    /// The first cell (0,0) is set as the start point of the algorithm
    /// The first region of the algorithm is also set.
    /// </summary>
    public MazeAlgorithm(MazeCell[,] cells, int mazeColumns, int mazeRows)
    {
        _cells = cells;
        _mazeColumns = mazeColumns;
        _mazeRows = mazeRows;

        passageways = new List<MazePassageway>();

        _cells[_currentX, _currentY].visited = true;

        _currentPassageway = new MazePassageway();
        _currentPassageway.AddCell(_cells[_currentX, _currentY]);
    }

    /// <summary>
    /// During this phase of the Algorithm, from the current cell a random direction is chosen (North, South, East, West)
    /// If the cell is avaliable (The cell hasn't been visited nor is it out of the grid width/height) the next cell is selected as the current cell and added to the passageway
    /// This repeats until the cell reaches a point where it cannot traverse to another cell since all of its adjacent cells are have been visited.
    /// </summary>
    public void Kill()
    {
        if (RouteStillAvaliable(_currentX, _currentY))
        {
            bool foundCell = false;
            while (!foundCell)
            {
                int direction = Random.Range(1, 5);

                if (direction == 1 && CellIsAvaliable(_currentX, _currentY - 1))
                {
                    _currentY--;
                    foundCell = true;
                }
                else if (direction == 2 && CellIsAvaliable(_currentX, _currentY + 1))
                {
                    _currentY++;
                    foundCell = true;
                }
                else if (direction == 3 && CellIsAvaliable(_currentX + 1, _currentY))
                {
                    _currentX++;
                    foundCell = true;
                }
                else if (direction == 4 && CellIsAvaliable(_currentX - 1, _currentY))
                {
                    _currentX--;
                    foundCell = true;
                }

                if (foundCell)
                {
                    _cells[_currentX, _currentY].visited = true;
                    _currentPassageway.AddCell(_cells[_currentX, _currentY]);
                }
            }
        }
        else
        {
            passageways.Add(_currentPassageway);
            _currentPassageway = new MazePassageway();
            Hunt();
        }
    }

    //During this phase the first cell that has not been visited, but has an adjacent cell that has been visited is found, and chosen as the start point of the next kill cycle.
    private void Hunt()
    {
        mazeComplete = true;

        for (int y = 0; y < _mazeRows; y++)
        {
            for (int x = 0; x < _mazeColumns; x++)
            {
                if (!_cells[x, y].visited && AdjacentVisitedCell(x, y))
                {
                    mazeComplete = false;
                    _currentX = x;
                    _currentY = y;
                    _cells[_currentX, _currentY].visited = true;
                    _currentPassageway.AddCell(_cells[_currentX, _currentY]);
                    return;
                }
            }
        }
    }

    /// <summary>
    /// This method checks are cell in location (x,y) to see if any adjacent cells have not been visited.
    /// If there is atleast one then the cell can traverse to that cell and the kill cycle can continue
    /// </summary>
    private bool RouteStillAvaliable(int x, int y)
    {
        int avaliableRoutes = 0;

        if (x > 0 && !_cells[x - 1, y].visited)
            avaliableRoutes++;

        if (x < _mazeColumns - 1 && !_cells[x + 1, y].visited)
            avaliableRoutes++;

        if (y > 0 && !_cells[x, y - 1].visited)
            avaliableRoutes++;

        if (y < _mazeRows - 1 && !_cells[x, y + 1].visited)
            avaliableRoutes++;

        return avaliableRoutes > 0;
    }

    /// <summary>
    /// Returns true is the cell (x, y) is within the grid's boundaries and has not been visited
    /// </summary>
    private bool CellIsAvaliable(int x, int y)
    {
        if (x >= 0 && x < _mazeColumns && y >= 0 && y < _mazeRows)
            return !_cells[x, y].visited;

        return false;
    }

    /// <summary>
    /// Returns true if the cell at location (x, y) has atleast one neighbour that has been visited
    /// </summary>
    private bool AdjacentVisitedCell(int x, int y)
    {
        int visitedCells = 0;

        if (x > 0 && _cells[x - 1, y].visited)
            visitedCells++;

        if (x < _mazeColumns - 2 && _cells[x + 1, y].visited)
            visitedCells++;

        if (y > 0 && _cells[x, y - 1].visited)
            visitedCells++;

        if (y < _mazeRows - 2 && _cells[x, y + 1].visited)
            visitedCells++;

        return visitedCells > 0;
    }
}
