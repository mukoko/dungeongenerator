﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Every Room, Region, Square in the maze/dungeon is formed by a MazeCell
/// This holds information of the floor and every wall of the cell
/// </summary>
public enum Direction
{
    North, South, East, West, Floor
}

public class MazeCell {

    /// <summary>
    /// Each wall and floor of a cell is formed by a MazeObject
    /// </summary>
    public class MazeObject
    {
        /// <summary>
        /// Variables:
        /// _outerWall : will be true if the cell is part of the outer wall x = 0, y = 0, x = mazeColumns, y = mazeRows
        /// _direction : The direction that the object faces
        /// _object : The gameObject that represents the cell
        /// _cell : A reference to the MazeCell that is Object is found in.
        /// </summary>
        /// 

        public bool exists { get; set; }
        public Direction direction { get; private set; }
        public GameObject mazeObject { get; set; }
        public MazeCell cell { get; private set; }

        public MazeObject(MazeCell cell, Direction direction)
        {
            this.direction = direction;
            this.cell = cell;
            exists = true;
        }

    }

    public MazeCell(int x, int y)
    {
        this.x = x;
        this.y = y;
        visited = false;
        unused = false;
    }

    /// <summary>
    /// _visited : If the cell has been visited by the generator algorithm or is part of a room
    /// _unused : If true the cell will be discarded
    /// _room : The room the cell belongs to (default = none)
    /// _pasageway : The pasageway the cell belongs to (default = none)
    /// </summary>

    public bool visited { get; set; }
    public bool unused { get; set; }
    public MazeObject northWall { get; set; }
    public MazeObject southWall { get; set; }
    public MazeObject eastWall { get; set; }
    public MazeObject westWall { get; set; }
    public MazeObject floor { get; set; }
    public MazeRoom room { get; set; }
    public MazePassageway passageway { get; set; }
    public int x { get; private set; }
    public int y { get; private set; }
}
